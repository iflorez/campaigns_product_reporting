#use this if you want to only clear outputs on select notebooks...and you want to specify those notebooks at run time
import nbformat
import sys

def clear_outputs(notebook_path: str):
    with open(notebook_path, 'r', encoding='utf-8') as file:
        nb = nbformat.read(file, as_version=4)
    for cell in nb.cells:
        if cell.cell_type == 'code':
            cell.outputs = []
            cell.execution_count = None
    with open(notebook_path, 'w', encoding='utf-8') as file:
        nbformat.write(nb, file)

def main():
    notebook_files = sys.argv[1:]  # Skip the first argument which is the script name
    for notebook_file in notebook_files:
        print(f"Clearing outputs for {notebook_file}")
        clear_outputs(notebook_file)
        print(f"Outputs cleared for {notebook_file}")

if __name__ == "__main__":
    main()